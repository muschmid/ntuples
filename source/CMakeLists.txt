# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.7 FATAL_ERROR )

# Set the project name
project(NTuples VERSION 1.0.0)

# Set AnalysisBase as default parent object
set( _defaultParentProject Athena )

# Configure ATLAS project
set( ATLAS_PROJECT ${_defaultParentProject}
   CACHE STRING "The name of the parent project to build against" )

# Clean up:
unset( _defaultParentProject )

# Find package
find_package( Athena REQUIRED )

# Test ATLAS setup
atlas_ctest_setup()

# ATLAS projects
atlas_project( UserAnalysis 1.0.0 USE ${ATLAS_PROJECT} ${${ATLAS_PROJECT}_VERSION})

# Create setup files
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )